User Management with

- Firebase
  - Sign In
  - Sign Up
  - Sign Out
  - Password Change
  - Password Reset

Protected Routes with

- NextJs and Firebase

Styling with

- Ant Design
- Styled Components
- Page Transitions

Type Support with

- TypeScript

Tested Code Base with

- Jest
- React Testing Library

Environment Variables with

- Dotenv

Absolute Imports with

- Babel Module Resolver
